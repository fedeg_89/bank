/* *** This is a fake Data Base ***

The following data would be queried from DB in the no demo app *** //

*/
const Database = {

    // Personal info 
    user: {
        id: 47794779,
        name: 'Jhon',
        lastName: 'Van Ketty',
        mail: 'jhonvanketty@busniess.com',
        phone: 18453124,
    },
    
    // Contact Diary
    contacts: [
        {name: 'John Smith', account: '65791164'},
        {name: 'Lara Machine', account: '98765414'},
        {name: 'Ernest Boy', account: '1234'}
    ],
    
    // Own accounts
    accounts: [
        {account: '456789987', type: 'Personal Checking Account', money: 45354},
        {account: '987481234', type: 'Business Checking Account', money: 15305}
    ],

    // Account history of movements table
    accountHistory: [
        {
          date: '02-22-2019',
          transaction: 'deposit',
          from: '98769876',
          destination: 'this',
          ammount: 1500
        },
        {
          date: '02-22-2019',
          transaction: 'extraction',
          from: 'this',
          destination: 'ATM',
          ammount: 250,
        },
        {
          date: '02-22-2019',
          transaction: 'buy of shares',
          from: 'this',
          destination: 'cuenta de inversiones',
          ammount: 800,
        },
        {
          date: '02-25-2019',
          transaction: 'transfer',
          from: 'this',
          destination: 'otra cuenta propia',
          ammount: 150
        },
        {
          date: '02-27-2019',
          transaction: 'transfer',
          from: 'this',
          destination: '54654622',
          ammount: 780
        }
      ],
    
    // What stocks te user have and how many of each
    stocks: {
        AAPL: 100,
        GOOGL: 130,
        MSFT: 78
    },

    // Investment History Table
    investmentHistory: [
        {symbol: 'AAPL', date: '02-20-2019', operation: 'buy', amount: 1123, purchasedPrice: 120, invested: 0},
        {symbol: 'AAPL', date: '02-20-2019', operation: 'buy', amount: 103, purchasedPrice: 130, invested: 0},
        {symbol: 'GOOGL', date: '02-21-2019', operation: 'buy', amount: 250, purchasedPrice: 100, invested: 0},
        {symbol: 'GOOGL', date: '02-22-2019', operation: 'buy', amount: 1050, purchasedPrice: 97, invested: 0},
        {symbol: 'GOOGL', date: '02-22-2019', operation: 'buy', amount: 113, purchasedPrice: 130, invested: 0},
        {symbol: 'GOOGL', date: '02-23-2019', operation: 'buy', amount: 203, purchasedPrice: 101, invested: 0},
        {symbol: 'MSFT', date: '02-25-2019', operation: 'buy', amount: 103, purchasedPrice: 120, invested: 0},
        {symbol: 'MSFT', date: '02-25-2019', operation: 'buy', amount: 780, purchasedPrice: 164, invested: 0},
        {symbol: 'MSFT', date: '02-27-2019', operation: 'buy', amount: 198, purchasedPrice: 141, invested: 0},
        {symbol: 'AAPL', date: '02-28-2019', operation: 'buy', amount: 510, purchasedPrice: 98, invested: 0}
    ],


    // Function to simulate a real query
    query: function query(table, column) {
        if (!column) {
            return this[table];
        } else {
            return this[table][column]
        }
    }
}

module.exports = Database;