// Modules 
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const Database = require('./database') // Fake database file

// Settings 
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'dist/bank')));

app.use(session({
    secret: 'kitty',
    resave: false,
    saveUninitialized: false
  }));

// Since it is a demo app with no DB, some data will be stored in the session.
app.use((req, res, next)=> {
    if (!req.session.accounts) {
        req.session.accounts = Database.accounts;
        req.session.stocks = Database.stocks;
        req.session.accountHistory = Database.accountHistory;
        req.session.investmentHistory = Database.investmentHistory;
        next();
    } else {
        next();
    }
})

// API's endpoints

app.get('/api/personalInfo', (req, res) => {
    const query = Database.query('personalInfo');
    res.send(query);
})

app.get('/api/contacts', (req, res) => {
    const query = Database.query('contacts');
    res.send(query);
})

app.get('/api/accounts', (req, res) => {
    res.send(req.session.accounts);
})

app.get('/api/accountHistory', (req, res) => {
    res.send(req.session.accountHistory)
})

app.get('/api/stocks', (req, res) => {
    res.send(req.session.stocks);
})

app.get('/api/investmentHistory', (req, res) => {
    res.send(req.session.investmentHistory);
})

app.patch('/api/accounts', (req, res) => {
    transfer(req.body, req);
    toAccountHistory(req.body, req);
    res.send({status: "ok"});
});

app.put('/api/stocks', (req, res) => {
    buyStocks(req.body, req)
    toInvestmentHistory(req.body, req)
    res.send({status: "ok"});
})

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/bank/index.html'))
});

// Functions

function transfer(formData, req) {
    const index = req.session.accounts.findIndex(account => account.account === formData.fromAccount.account);
        // Finds the account where the transaction was made
    req.session.accounts[index].money -= formData.amount;
        // Makes the transaction
    if (formData.toAccount) {
        ownAccountIndex = req.session.accounts.findIndex(account => account.account == formData.toAccount); // "==" because one is number and the other is string.
            // Checks if the destination account owns to user
        if (ownAccountIndex != -1) {
            req.session.accounts[ownAccountIndex].money += formData.amount;
        }
    }
}      

function toAccountHistory(formData, req) {
    const transaction = {
        date: new Date(),
        transaction: 'transfer',
        from: formData.fromAccount.account,
        destination: formData.toAccount,
        ammount: formData.amount
    }
    req.session.accountHistory = [transaction, ...req.session.accountHistory];
}

function buyStocks(formData, req) {
    req.session.stocks[formData.symbol] += formData.stocks;
    transfer(formData, req);
}

function toInvestmentHistory(formData, req) {
    const stockTransaction = {
        symbol: formData.symbol,
        date: new Date(),
        operation: formData.operation,
        amount: formData.stocks,
        purchasedPrice: formData.latestPrice,
        invested: 0,
        value: 0,
        profit: 0,
        profitP: 0
    }
    req.session.investmentHistory = [stockTransaction, ...req.session.investmentHistory];
}


const port = process.env.port || 5000;
app.listen(port, () =>
    console.log(`Server started at port: ${port}`)
);
