import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AccountService } from '../shared/services/account.service';
import { AccountHistory } from '../shared/models/account-history';
import { Accounts } from '../shared/models/accounts';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'bank-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {

  displayedColumns: string[]; // = ['date', 'transaction', 'from', 'destination', 'ammount'];


  accounts: Accounts;
  history: AccountHistory;

  currentAccount: string;
  currentType: string;
  currentMoney: number;

  selectAccount = new FormControl('');

  constructor(private accountService: AccountService) { }

  ngOnInit() {
    // Get the data from server
    this.accountService.getAccountHistory()
      .subscribe(res => {
        this.displayedColumns = Object.keys(res[0]);
        this.history = res;
      });
    this.accountService.getAccounts().subscribe(res => {
      this.selectAccount.setValue(res[0]); // Set the select on first account
      this.accounts = res;
    });

    // Account select
    this.selectAccount.valueChanges.subscribe(value => {
     this.currentAccount = value.account;
     this.currentType = value.type;
     this.currentMoney = value.money;
    });
  }

  setCurrencyStyles(curr) {
    const classes = {
      positive: curr > 0,
      negative: curr < 0
    };
    return classes;
  }
}
