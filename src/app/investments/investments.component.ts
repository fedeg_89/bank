import { Component, OnInit } from '@angular/core';
import { StocksService } from '../shared/services/stocks.service';
import { InvestmentHistoryService } from '../shared/services/investment-history.service';
import { InvestmentHistory } from '../shared/models/investment-history';

@Component({
  selector: 'bank-investments',
  templateUrl: './investments.component.html',
  styleUrls: ['./investments.component.css']
})
export class InvestmentsComponent implements OnInit {

  displayedColumns: string[];

  investmentHistory: InvestmentHistory;
  stocks; // no hay modelo de este, es el que trae la api
  userStocks;

  constructor(
    private stockService: StocksService,
    private investmentHistoryService: InvestmentHistoryService
    ) { }

  setValues() {
    this.investmentHistory.forEach(row => {
      row.invested = row.amount * row.purchasedPrice;
      const index = this.stocks.findIndex(stock => stock.symbol === row.symbol);
      this.stocks[index].totalInvested += row.invested;
    });
    console.log(this.stocks);
  }

  getStocks() {
    this.stockService.getStocks()
      .subscribe(stocksFromApi => {
        this.stocks = Object.keys(stocksFromApi).map(key => {
          const stocksArray = stocksFromApi[key].quote;
          stocksArray.logo = stocksFromApi[key].logo.url;
          stocksArray.totalInvested = 0;
          return stocksArray;
        });
        console.log(this.stocks);
        this.setValues();
      });
  }

  getUserStocks(): void {
    this.stockService.getUserStocks().subscribe(res => this.userStocks = res);
  }

  getInvestmentHistory(): void {
    this.investmentHistoryService.getInvestmentHistory().subscribe(res => {
      this.displayedColumns = Object.keys(res[0]);
      this.investmentHistory = res;
    });
  }

  // async ejecutar() {
  //   await this.getStocks();
  //   console.log(this.stocks);
  // }

  ngOnInit() {
    // this.ejecutar();
    this.getStocks();
    this.getUserStocks();
    this.getInvestmentHistory();
    // this.setValues();
  }

  setCurrencyStyles(curr) {
    const classes = {
      positive: curr > 0,
      negative: curr < 0
    };
    return classes;
  }

}
