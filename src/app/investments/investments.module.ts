import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { InvestmentsComponent } from './investments.component';

import { MaterialModule } from '../material/material.module';
import { InvestmentCardComponent } from '../investment-card/investment-card.component';
import { StocksComponent } from '../stocks/stocks.component';

@NgModule({
  declarations: [InvestmentsComponent, InvestmentCardComponent, StocksComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  entryComponents: [StocksComponent]
})
export class InvestmentsModule { }
