export interface Contacts extends Array<any> {
    [index: number]: { name: string, account: string};
}
