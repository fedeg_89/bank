export interface Stocks {
    [symbol: string]: number;
}
