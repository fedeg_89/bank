export interface AccountHistory {
    [index: number]: {
        date: Date,
        transaction: string,
        from: string,
        destination: string,
        amount: number
        };
}
