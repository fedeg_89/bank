export interface InvestmentHistory extends Array<any> {
    [index: number]: {
        symbol: string,
        date: Date,
        operation: string,
        amount: number,
        purchasedPrice: number,
        invested: number
       };
}
