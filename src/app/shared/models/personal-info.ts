export interface PersonalInfo {
    id: number;
    name: string;
    lastName: string;
    mail: string;
    phone: number;
}
