export interface Accounts {
    [index: number]: {account: string, type: string, money: number};
}
