import { TestBed } from '@angular/core/testing';

import { InvestmentHistoryService } from './investment-history.service';

describe('InvestmentHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InvestmentHistoryService = TestBed.get(InvestmentHistoryService);
    expect(service).toBeTruthy();
  });
});
