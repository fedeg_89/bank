import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { Stocks } from '../models/stocks';

@Injectable({
  providedIn: 'root'
})
export class StocksService {

  // symbols: string[] = Object.keys(this.stocks);
  symbols: string[] = ['aapl', 'googl', 'msft'];
  symbolsQuery = this.symbols.toString().toLowerCase();
  stockApiUrl = 'https://api.iextrading.com/1.0/stock/market/batch?symbols=';
  stockApiTypes = '&types=quote,logo';

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  getStocks(): Observable<object> {
    return this.http.get<object>(this.stockApiUrl + this.symbolsQuery + this.stockApiTypes)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  getUserStocks(): Observable<Stocks> {
    return this.http.get<Stocks>('/api/stocks')
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  buyStocks(formData, symbol, operation): Observable<object> {
    formData.symbol = symbol;
    formData.operation = operation;
    return this.http.put<object>('/api/stocks', formData)
    .pipe(
      catchError(this.handleError)
    );
  }
}
