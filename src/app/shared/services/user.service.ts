import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { PersonalInfo } from '../models/personal-info';
import { Contacts } from '../models/contacts';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  getPersonalInfo(): Observable<PersonalInfo> {
    return this.http.get<PersonalInfo>('/api/personalInfo')
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  getContacts(): Observable<Contacts> {
    return this.http.get<Contacts>('/api/contacts')
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }
}
