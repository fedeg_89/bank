import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { Accounts } from '../models/accounts';
import { AccountHistory } from '../models/account-history';


@Injectable({
  providedIn: 'root'
})
export class AccountService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  getAccounts(): Observable<Accounts> {
    return this.http.get<Accounts>('/api/accounts')
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  getAccountHistory(): Observable<AccountHistory> {
    return this.http.get<AccountHistory>('/api/accountHistory')
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  onTransfer(formData): Observable<any> {
    return this.http.patch('/api/accounts', formData, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    );
  }
}
