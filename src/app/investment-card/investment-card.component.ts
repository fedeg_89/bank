import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { StocksComponent } from '../stocks/stocks.component';
import { InvestmentsComponent } from '../investments/investments.component';

@Component({
  selector: 'bank-investment-card',
  templateUrl: './investment-card.component.html',
  styleUrls: ['./investment-card.component.css']
})
export class InvestmentCardComponent implements OnInit {

  @Input() symbol: string;
  @Input() companyName: string;
  @Input() logo: string;
  @Input() latestPrice: number;
  @Input() change: number;
  @Input() changePercent: number;
  @Input() totalInvested: number;
  @Input() userStocks: object;

  profit: number;


  constructor(public dialog: MatDialog, private investmentsComponent: InvestmentsComponent) { }

  openStocks(operation): void {
    const dialogRef = this.dialog.open(StocksComponent, {
      width: '980px',
      data: {
        symbol: this.symbol,
        amount: this.userStocks[this.symbol],
        latestPrice: this.latestPrice,
        operation
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.investmentsComponent.getUserStocks();
      console.log('reiniciar comp');
      // console.log(result);
      // this.animal = result;
    });
  }

  ngOnInit() {
    this.profit = this.totalInvested - (this.userStocks[this.symbol] * this.latestPrice);
  }

}
