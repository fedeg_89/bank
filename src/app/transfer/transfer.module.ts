import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { TransferComponent } from './transfer.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [TransferComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class TransferModule { }
