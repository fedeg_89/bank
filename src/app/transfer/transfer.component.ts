import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AccountService } from '../shared/services/account.service';
import { UserService } from '../shared/services/user.service';
import { Accounts } from '../shared/models/accounts';
import { Contacts } from '../shared/models/contacts';
import { Router } from '@angular/router';

@Component({
  selector: 'bank-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {

  accounts: Accounts;
  contacts: Contacts;

  money: number;

  transferForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private accountService: AccountService,
    private userService: UserService,
    private router: Router
    ) { }

  ngOnInit() {
    // Get the data from server
    this.accountService.getAccounts().subscribe(res => this.accounts = res);
    this.userService.getContacts().subscribe(res => this.contacts = res);

    // The form
    this.transferForm = this.fb.group({
      fromAccount: ['', Validators.required],
      toAccount: ['', Validators.required],
      amount: ['', [Validators.required, Validators.max(this.money)]],
      fromDiary: ''
    });

    // Dynamic validator: set the max validator from money on selected account
    this.transferForm.controls.fromAccount.valueChanges.subscribe(accountSelected => {
      this.money = accountSelected.money;
      this.transferForm.controls.amount.setValidators([Validators.required, Validators.max(this.money)]);
      this.transferForm.controls.amount.updateValueAndValidity();
    });

    // Auto fill the number "to account" when user selects from his contacts
    this.transferForm.controls.fromDiary.valueChanges.subscribe(value => {
      this.transferForm.controls.toAccount.setValue(value.account);
    });

/******* ROMPE ******
    // Fills the Diary if typed account match with an existing conctact
    this.transferForm.controls.toAccount.valueChanges.subscribe(value => {
      const newAccountName = this.contacts.find(contact => contact.account === value.toString());
      if (newAccountName) {
        this.transferForm.controls.fromDiary.setValue(newAccountName);
      }
    });
    *******     ******/
  }

  // Form's controls
  get fromAccount() {
    return this.transferForm.get('fromAccount');
  }
  get toAccount() {
    return this.transferForm.get('toAccount');
  }
  get amount() {
    return this.transferForm.get('amount');
  }
  get fromDiary() {
    return this.transferForm.get('fromDiary');
  }

  // Submit
  onTransfer() {
    this.accountService.onTransfer(this.transferForm.value).subscribe(res => {
      if (res.status === 'ok') {
        this.router.navigate(['/accounts']);
      }
    });
  }

}
