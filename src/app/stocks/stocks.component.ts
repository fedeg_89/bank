import { Component, OnInit, Inject} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Accounts } from '../shared/models/accounts';
import { StocksService } from '../shared/services/stocks.service';
import { AccountService } from '../shared/services/account.service';

@Component({
  selector: 'bank-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {

  VARstocks = 1;
  VARamount = this.VARstocks * this.data.latestPrice;
  accounts: Accounts;

  stocksForm: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<StocksComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private stocksService: StocksService,
    private accountService: AccountService
    ) { }

  buyStocks(): void {
    this.stocksService.buyStocks(this.stocksForm.value, this.data.symbol, this.data.operation).subscribe(res => {
      console.log('You bought stocks!');
      this.dialogRef.close();
    });
  }

  sellStocks(): void {
    const stocksForm = this.stocksForm.value;
    stocksForm.stocks = -stocksForm.stocks;
    stocksForm.amount = -stocksForm.amount;
    this.stocksService.buyStocks(stocksForm, this.data.symbol, this.data.operation).subscribe(res => {
      console.log('You sold stocks!');
      this.dialogRef.close();
    });
  }

  cancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.stocksForm = this.fb.group({
      latestPrice: this.data.latestPrice,
      stocks: ['', Validators.required],
      fromAccount: ['', Validators.required],
      amount: ['', Validators.max(5000),
    ]
  });

    this.stocksForm.controls.stocks.valueChanges.subscribe(stocks => {
        this.VARamount = this.data.latestPrice * stocks;
        this.stocksForm.controls.amount.setValue(this.VARamount.toFixed(2));
        this.stocksForm.controls.amount.updateValueAndValidity();

    });

    // this.stocksForm.controls.amount.valueChanges.subscribe(amount => {
    //     this.VARstocks = amount / this.data.latestPrice;
    //     this.stocksForm.controls.stocks.setValue(this.VARstocks);
    //     this.stocksForm.controls.stocks.updateValueAndValidity();
    // });

    this.accountService.getAccounts().subscribe(res => this.accounts = res);

}

  get latestPrice() {
    return this.stocksForm.get('latestPrice');
  }

  get stocks() {
    return this.stocksForm.get('stocks');
  }

  get fromAccount() {
    return this.stocksForm.get('fromAccount');
  }

  get amount() {
    return this.stocksForm.get('amount');
  }

}
